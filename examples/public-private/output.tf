output "vpc" {
  description = "VPC ID"
  value       = module.vpc.vpc
}

output "vpc_cidr" {
  description = "VPC CIDR"
  value       = module.vpc.vpc_cidr
}

output "account_owner" {
  description = "Account ID"
  value       = module.vpc.vpc_owner
}

output "subnets_public_az" {
  description = "List of IDs of subnets_private"
  value       = module.vpc.subnet_public_ids
}

output "subnets_private_az" {
  description = "List of IDs of subnets_private"
  value       = module.vpc.subnet_private_ids
}