module "vpc" {
  source = "../../modules/subnet-public-private"

  client_name = "pcnunes"
  region      = "us-east-1"
  environment = "labs"
  num_azs     = 2

  vpc_cidr = {
    range = "10.10.0.0/20"
  }

  subnet_mask = {
    public  = 4
    private = 4
  }

  subnet_mask_init = {
    public  = 0
    private = 2
  }

  create_database_subnet_group = {
    public  = true
    private = true
  }

}
