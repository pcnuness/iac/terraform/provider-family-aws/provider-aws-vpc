module "vpc" {
  source = "git::https://gitlab.com/pcnuness/iac/terraform/provider-family-aws/provider-aws-vpc.git//modules/subnet-public-private-database-publish?ref=v1.0.0"

  region      = "us-east-1"
  environment = "modulo-vpc"
  num_azs     = 3

  vpc_cidr = {
    range = "10.0.0.0/20"
  }

  subnet_mask = {
    private  = 4
    public   = 4
    database = 4
    publish  = 3
  }

  subnet_mask_init = {
    publish  = 0
    private  = 6
    public   = 9
    database = 12
  }

  create_database_subnet_route_tables = true
  create_database_nat_gateway         = true
  create_database_internet_gateway    = false
  create_database_subnet_group = {
    private = true
    public  = true
  }

}
