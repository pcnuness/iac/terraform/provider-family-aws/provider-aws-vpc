## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 0.14 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 5.31.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | = 2.4.1 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | git::https://gitlab.com/pcnuness/iac/terraform/provider-family-aws/provider-aws-vpc.git//modules/subnet-public-private-database-publish?ref=v1.0.0 | v1.0.0 |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_account_owner"></a> [account\_owner](#output\_account\_owner) | Account ID |
| <a name="output_subnets_database_az"></a> [subnets\_database\_az](#output\_subnets\_database\_az) | List of IDs of subnets\_database |
| <a name="output_subnets_private_az"></a> [subnets\_private\_az](#output\_subnets\_private\_az) | List of IDs of subnets\_private |
| <a name="output_subnets_public_az"></a> [subnets\_public\_az](#output\_subnets\_public\_az) | List of IDs of subnets\_private |
| <a name="output_subnets_publish_az"></a> [subnets\_publish\_az](#output\_subnets\_publish\_az) | List of IDs of subnets\_publish |
| <a name="output_vpc"></a> [vpc](#output\_vpc) | VPC ID |
| <a name="output_vpc_cidr"></a> [vpc\_cidr](#output\_vpc\_cidr) | VPC CIDR |

## InfraCost
```
 Name                                                           Monthly Qty  Unit              Monthly Cost 
                                                                                                            
 module.vpc.module.vpc.aws_cloudwatch_log_group.flow_log[0]                                                 
 ├─ Data ingested                                            Monthly cost depends on usage: $0.50 per GB    
 ├─ Archival Storage                                         Monthly cost depends on usage: $0.03 per GB    
 └─ Insights queries data scanned                            Monthly cost depends on usage: $0.005 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[0]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[1]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[2]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 OVERALL TOTAL                                                                                       $98.55 
──────────────────────────────────
60 cloud resources were detected:
∙ 4 were estimated, all of which include usage-based costs, see https://infracost.io/usage-file
∙ 56 were free:
  ∙ 12 x aws_route_table_association
  ∙ 12 x aws_subnet
  ∙ 8 x aws_route_table
  ∙ 7 x aws_route
  ∙ 3 x aws_eip
  ∙ 3 x aws_security_group
  ∙ 2 x aws_db_subnet_group
  ∙ 1 x aws_default_network_acl
  ∙ 1 x aws_default_route_table
  ∙ 1 x aws_default_security_group
  ∙ 1 x aws_flow_log
  ∙ 1 x aws_iam_policy
  ∙ 1 x aws_iam_role
  ∙ 1 x aws_iam_role_policy_attachment
  ∙ 1 x aws_internet_gateway
  ∙ 1 x aws_vpc

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┓
┃ Project                                                          ┃ Monthly cost ┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━┫
┃ provider-aws-vpc/modules/subnet-public-private-database-publish  ┃ $99          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━┛
```