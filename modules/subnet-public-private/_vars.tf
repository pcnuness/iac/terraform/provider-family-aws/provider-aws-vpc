variable "region" {
  description = "AWS Landing Zone"
  default     = ""
}

variable "client_name" {
  description = "Client Name"
  type        = string
  default     = ""
}

variable "environment" {
  description = "AWS Environment"
  type        = string
  default     = ""
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  type        = map(any)
  default = {
    range = ""
  }
}

variable "num_azs" {
  description = "Number of AZs"
  default     = ""
}

variable "subnet_mask" {
  description = "Subnet Mask Range"
  type        = map(any)
  default = {
    public   = ""
    private  = ""
    database = ""
    publish  = "" # Using 2 or 3
  }
}

variable "subnet_mask_init" {
  description = "Subnet Mask Range Initial"
  type        = map(any)
  default = {
    public   = ""
    private  = ""
    database = ""
    publish  = ""
  }
}

variable "create_database_subnet_group" {
  description = "Subnet Group Create Public or Private"
  type        = map(any)
  default = {
    public  = ""
    private = ""
  }
}