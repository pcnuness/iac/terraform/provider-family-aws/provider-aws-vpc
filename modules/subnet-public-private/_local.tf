locals {
  # ==================================================================
  # GENERAL
  # ==================================================================
  client_name  = var.client_name
  environment  = var.environment
  account_name = "${local.client_name}-${local.environment}-services"
  account_id   = data.aws_caller_identity.current.account_id
  region       = var.region


  # ==================================================================
  # VPC
  # ==================================================================
  vpc_name                  = "${local.environment}-services"
  num_azs                   = var.num_azs
  azs                       = slice(data.aws_availability_zones.available.names, 0, local.num_azs)
  subnet_private_prefix     = "subnet-${local.vpc_name}-private"
  subnet_public_prefix      = "subnet-${local.vpc_name}-public"
  subnet_group_prefix       = local.vpc_name
  private_subnet_group_name = "${local.subnet_group_prefix}-private"
  public_subnet_group_name  = "${local.subnet_group_prefix}-public"

  # ==================================================================
  # Network
  # ==================================================================

  vpc_cidr = {
    range = var.vpc_cidr.range
  }

  subnet_mask = {
    private  = var.subnet_mask.private
    public   = var.subnet_mask.public
  }

  subnet_mask_init = {
    private  = var.subnet_mask_init.private
    public   = var.subnet_mask_init.public
  }

  create_database_subnet_group = {
    private = var.create_database_subnet_group.private
    public  = var.create_database_subnet_group.public
  }

}