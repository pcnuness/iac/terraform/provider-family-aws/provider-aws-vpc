provider "aws" {
  region = var.region
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.1"

  name                   = local.vpc_name
  cidr                   = var.vpc_cidr.range
  azs                    = local.azs
  public_subnets         = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.public, k + local.subnet_mask_init.public)]
  private_subnets        = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.private, k + local.subnet_mask_init.private)]
  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
  enable_dns_hostnames   = true
  enable_dns_support     = true

  # Flow Logs
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60
}

resource "aws_db_subnet_group" "public" {
  count = local.create_database_subnet_group.public ? 1 : 0

  name        = lower(coalesce(local.public_subnet_group_name))
  description = "Public subnet group for ${local.vpc_name}"
  subnet_ids  = module.vpc.public_subnets

  tags = merge(
    {
      "Name" = lower(coalesce(local.public_subnet_group_name))
    },
  )
}

resource "aws_db_subnet_group" "private" {
  count = local.create_database_subnet_group.private ? 1 : 0

  name        = lower(coalesce(local.private_subnet_group_name))
  description = "Private subnet group for ${local.vpc_name}"
  subnet_ids  = module.vpc.private_subnets

  tags = merge(
    {
      "Name" = lower(coalesce(local.private_subnet_group_name))
    },
  )
}