## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 0.14 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 5.31.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | = 2.4.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | = 5.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 2.66.0 |

## Resources

| Name | Type |
|------|------|
| [aws_db_subnet_group.private](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/db_subnet_group) | resource |
| [aws_db_subnet_group.public](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/db_subnet_group) | resource |
| [aws_security_group.sg_global_network](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_security_group.sg_rds_mysql](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_security_group.sg_rds_postgres](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_acm_certificate.issued](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/acm_certificate) | data source |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/availability_zones) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/region) | data source |
| [aws_route53_zone.selected](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_database_subnet_group"></a> [create\_database\_subnet\_group](#input\_create\_database\_subnet\_group) | Subnet Group Create | `string` | `""` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | AWS Environment | `string` | `""` | no |
| <a name="input_num_azs"></a> [num\_azs](#input\_num\_azs) | Number of AZs | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS Landing Zone | `string` | `""` | no |
| <a name="input_subnet_mask"></a> [subnet\_mask](#input\_subnet\_mask) | Subnet Mask Range | `map(any)` | <pre>{<br>  "database": "",<br>  "private": "",<br>  "public": "",<br>  "publish": ""<br>}</pre> | no |
| <a name="input_subnet_mask_init"></a> [subnet\_mask\_init](#input\_subnet\_mask\_init) | Subnet Mask Range Initial | `map(any)` | <pre>{<br>  "database": "",<br>  "private": "",<br>  "public": "",<br>  "publish": ""<br>}</pre> | no |
| <a name="input_type_database_subnet_group"></a> [type\_database\_subnet\_group](#input\_type\_database\_subnet\_group) | Subnet Group Create Public or Private | `map(any)` | <pre>{<br>  "private": "",<br>  "public": ""<br>}</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC CIDR | `map(any)` | <pre>{<br>  "range": ""<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_igw"></a> [igw](#output\_igw) | The ID of the Internet Gateway |
| <a name="output_region"></a> [region](#output\_region) | AWS region |
| <a name="output_subnet_private_ids"></a> [subnet\_private\_ids](#output\_subnet\_private\_ids) | List of IDs of private subnets |
| <a name="output_subnet_public_ids"></a> [subnet\_public\_ids](#output\_subnet\_public\_ids) | List of IDs of public subnets |
| <a name="output_vpc"></a> [vpc](#output\_vpc) | VPC ID |
| <a name="output_vpc_cidr"></a> [vpc\_cidr](#output\_vpc\_cidr) | VPC ID |
| <a name="output_vpc_owner"></a> [vpc\_owner](#output\_vpc\_owner) | VPC ID |
