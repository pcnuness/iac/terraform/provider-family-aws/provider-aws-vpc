## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 0.14 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 5.31.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | = 2.4.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | = 5.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 5.4.0 |

## Resources

| Name | Type |
|------|------|
| [aws_db_subnet_group.private](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/db_subnet_group) | resource |
| [aws_db_subnet_group.public](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/db_subnet_group) | resource |
| [aws_db_subnet_group.public_igw](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/db_subnet_group) | resource |
| [aws_security_group.sg_global_network](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_security_group.sg_rds_mysql](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_security_group.sg_rds_postgres](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/resources/security_group) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/5.31.0/docs/data-sources/availability_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_database_internet_gateway"></a> [create\_database\_internet\_gateway](#input\_create\_database\_internet\_gateway) | Database Subnet using Internet Gateway | `string` | `""` | no |
| <a name="input_create_database_nat_gateway"></a> [create\_database\_nat\_gateway](#input\_create\_database\_nat\_gateway) | Database Subnet using Nat Gateway by AZ's | `string` | `""` | no |
| <a name="input_create_database_subnet_group"></a> [create\_database\_subnet\_group](#input\_create\_database\_subnet\_group) | Database Subnet Group | `map(any)` | <pre>{<br>  "private": "",<br>  "public": ""<br>}</pre> | no |
| <a name="input_create_database_subnet_route_tables"></a> [create\_database\_subnet\_route\_tables](#input\_create\_database\_subnet\_route\_tables) | Database route table by AZ's | `string` | `""` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | VPC name | `string` | `""` | no |
| <a name="input_num_azs"></a> [num\_azs](#input\_num\_azs) | Number of AZs | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS Landing Zone | `string` | `""` | no |
| <a name="input_subnet_mask"></a> [subnet\_mask](#input\_subnet\_mask) | Subnet Mask Range | `map(any)` | <pre>{<br>  "database": "",<br>  "private": "",<br>  "public": "",<br>  "publish": ""<br>}</pre> | no |
| <a name="input_subnet_mask_init"></a> [subnet\_mask\_init](#input\_subnet\_mask\_init) | Subnet Mask Range | `map(any)` | <pre>{<br>  "database": "",<br>  "private": "",<br>  "public": "",<br>  "publish": ""<br>}</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC CIDR | `map(any)` | <pre>{<br>  "range": ""<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_account_owner"></a> [account\_owner](#output\_account\_owner) | VPC ID |
| <a name="output_igw"></a> [igw](#output\_igw) | The ID of the Internet Gateway |
| <a name="output_region"></a> [region](#output\_region) | AWS region |
| <a name="output_subnet_database_ids"></a> [subnet\_database\_ids](#output\_subnet\_database\_ids) | List of IDs of private subnets |
| <a name="output_subnet_private_ids"></a> [subnet\_private\_ids](#output\_subnet\_private\_ids) | List of IDs of private subnets |
| <a name="output_subnet_public_ids"></a> [subnet\_public\_ids](#output\_subnet\_public\_ids) | List of IDs of public subnets |
| <a name="output_subnet_publish_ids"></a> [subnet\_publish\_ids](#output\_subnet\_publish\_ids) | List of IDs of private subnets |
| <a name="output_vpc"></a> [vpc](#output\_vpc) | VPC ID |
| <a name="output_vpc_cidr"></a> [vpc\_cidr](#output\_vpc\_cidr) | VPC ID |

