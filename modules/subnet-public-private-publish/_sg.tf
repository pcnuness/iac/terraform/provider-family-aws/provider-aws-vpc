resource "aws_security_group" "sg_rds_postgres" {
  name_prefix = "sg_rds_postgres"
  description = "Allow PostgreSQL inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  tags = {
    Name = "sg-rds-postgres"
  }
}

resource "aws_security_group" "sg_rds_mysql" {
  name_prefix = "sg_rds_mysql"
  description = "Allow Mysql inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  tags = {
    Name = "sg-rds-mysql"
  }
}

resource "aws_security_group" "sg_global_network" {
  name_prefix = "sg_global_network"
  description = "Allow Global Network traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  tags = {
    Name = "sg-global-network"
  }
}