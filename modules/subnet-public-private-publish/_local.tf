locals {
  region                      = var.region
  num_azs                     = var.num_azs
  azs                         = slice(data.aws_availability_zones.available.names, 0, local.num_azs)
  vpc_name                    = "${var.environment}-services"
  subnet_prefix               = local.vpc_name
  database_subnet_suffix      = "${local.vpc_name}-database"
  intra_subnet_suffix         = "${local.vpc_name}-publish"
  private_subnet_group_prefix = "private-${local.vpc_name}"
  public_subnet_group_prefix  = "public-${local.vpc_name}"

  # Network
  vpc_cidr = {
    range = var.vpc_cidr.range
  }

  subnet_mask = {
    private  = var.subnet_mask.private
    public   = var.subnet_mask.public
    database = var.subnet_mask.database
    publish  = var.subnet_mask.publish
  }

  subnet_mask_init = {
    private  = var.subnet_mask_init.private
    public   = var.subnet_mask_init.public
    database = var.subnet_mask_init.database
    publish  = var.subnet_mask_init.publish
  }

  create_database_subnet_route_tables = var.create_database_subnet_route_tables
  create_database_nat_gateway         = var.create_database_nat_gateway
  create_database_internet_gateway    = var.create_database_internet_gateway
  create_database_subnet_group = {
    private = var.create_database_subnet_group.private
    public  = var.create_database_subnet_group.public
  }

}