################################################################################
# VPC
################################################################################

output "region" {
  description = "AWS region"
  value       = var.region
}

output "vpc" {
  description = "VPC ID"
  value       = module.vpc.vpc_id
}

output "vpc_cidr" {
  description = "VPC ID"
  value       = module.vpc.vpc_cidr_block
}

output "account_owner" {
  description = "VPC ID"
  value       = module.vpc.vpc_owner_id
}

################################################################################
# Internet Gateway
################################################################################

output "igw" {
  description = "The ID of the Internet Gateway"
  value       = module.vpc.igw_id
}

################################################################################
# Private
################################################################################

output "subnet_private_ids" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets
}

################################################################################
# Publiс
################################################################################

output "subnet_public_ids" {
  description = "List of IDs of public subnets"
  value       = module.vpc.public_subnets
}

################################################################################
# Database
################################################################################

output "subnet_database_ids" {
  description = "List of IDs of private subnets"
  value       = module.vpc.database_subnets
}

################################################################################
# Intra
################################################################################

output "subnet_publish_ids" {
  description = "List of IDs of private subnets"
  value       = module.vpc.intra_subnets
}