module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  name = local.vpc_name
  cidr = local.vpc_cidr.range
  azs  = local.azs

  intra_subnets    = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.publish, k + local.subnet_mask_init.publish)]
  private_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.private, k + local.subnet_mask_init.private)]
  public_subnets   = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.public, k + local.subnet_mask_init.public)]
  database_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr.range, local.subnet_mask.database, k + local.subnet_mask_init.database)]

  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
  enable_dns_hostnames   = true
  enable_dns_support     = true

  # Flow Logs
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60

  # Database
  create_database_subnet_route_table     = local.create_database_subnet_route_tables
  create_database_subnet_group           = false
  create_database_nat_gateway_route      = local.create_database_nat_gateway
  create_database_internet_gateway_route = local.create_database_internet_gateway

  database_subnet_suffix = "database"
  database_subnet_names = [
    "${local.database_subnet_suffix}-${local.azs[0]}",
    "${local.database_subnet_suffix}-${local.azs[1]}",
    "${local.database_subnet_suffix}-${local.azs[2]}"
  ]

  # Publish
  intra_subnet_suffix = "intra"
  intra_subnet_names = [
    "${local.intra_subnet_suffix}-${local.azs[0]}",
    "${local.intra_subnet_suffix}-${local.azs[1]}",
    "${local.intra_subnet_suffix}-${local.azs[2]}"
  ]

  intra_route_table_tags = {
    Name = "${local.intra_subnet_suffix}"
  }

  intra_subnet_tags = {
    "kubernetes.io/cluster/eks" = true
  }

}

resource "aws_db_subnet_group" "private" {
  count       = local.create_database_subnet_group.private ? !local.create_database_internet_gateway && local.create_database_subnet_group.public ? 1 : 0 : 1
  name        = lower(coalesce(local.private_subnet_group_prefix))
  description = "Private subnet group for ${local.vpc_name}"
  subnet_ids  = module.vpc.database_subnets

  tags = merge(
    {
      "Name" = lower(coalesce(local.private_subnet_group_prefix))
    },
  )
}

resource "aws_db_subnet_group" "public" {
  count = local.create_database_subnet_group.public ? !local.create_database_internet_gateway && local.create_database_subnet_group.private ? 1 : 0 : 1

  name        = lower(coalesce(local.public_subnet_group_prefix))
  description = "Public subnet group for ${local.vpc_name}"
  subnet_ids  = module.vpc.public_subnets

  tags = merge(
    {
      "Name" = lower(coalesce(local.public_subnet_group_prefix))
    },
  )
}

resource "aws_db_subnet_group" "public_igw" {
  count = local.create_database_internet_gateway && local.create_database_subnet_group.public ? 1 : 0

  name        = lower(coalesce(local.public_subnet_group_prefix))
  description = "Public subnet group for ${local.vpc_name}"
  subnet_ids  = module.vpc.database_subnets

  tags = merge(
    {
      "Name" = lower(coalesce(local.public_subnet_group_prefix))
    },
  )
}