provider "aws" {
  region = local.region

  default_tags {
    tags = {
      "automation:managedby" = "terraform"
    }
  }
}