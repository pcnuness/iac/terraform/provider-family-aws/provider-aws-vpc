variable "region" {
  description = "AWS Landing Zone"
  default     = ""
}

variable "environment" {
  description = "VPC name"
  type        = string
  default     = ""
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  type        = map(any)
  default = {
    range = ""
  }
}

variable "num_azs" {
  description = "Number of AZs"
  default     = ""
}

variable "subnet_mask" {
  description = "Subnet Mask Range"
  type        = map(any)
  default = {
    private  = ""
    public   = ""
    database = ""
    publish  = "" # Using 2 or 3
  }
}

variable "subnet_mask_init" {
  description = "Subnet Mask Range"
  type        = map(any)
  default = {
    private  = ""
    public   = ""
    database = ""
    publish  = ""
  }
}

variable "create_database_subnet_group" {
  description = "Database Subnet Group"
  type        = map(any)
  default = {
    private = ""
    public  = ""
  }
}

variable "create_database_subnet_route_tables" {
  description = "Database route table by AZ's"
  default     = ""
}

variable "create_database_nat_gateway" {
  description = "Database Subnet using Nat Gateway by AZ's"
  default     = ""
}

variable "create_database_internet_gateway" {
  description = "Database Subnet using Internet Gateway"
  default     = ""
}
