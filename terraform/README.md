## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.5.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 5.35.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | = 2.4.1 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | ../modules/subnet-public-private-publish | n/a |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_subnets_private_az"></a> [subnets\_private\_az](#output\_subnets\_private\_az) | List of IDs of subnets\_private |
| <a name="output_subnets_public_az"></a> [subnets\_public\_az](#output\_subnets\_public\_az) | List of IDs of subnets\_private |
| <a name="output_vpc"></a> [vpc](#output\_vpc) | VPC ID |
| <a name="output_vpc_cidr"></a> [vpc\_cidr](#output\_vpc\_cidr) | VPC CIDR |
