module "vpc" {
  source = "git::https://gitlab.com/pcnuness/cpe/cpn/aws.git//vpc/modules/subnet-public-private-database-publish?ref=main"

  region      = "us-east-1"
  environment = "demo-labs"
  num_azs     = 2

  vpc_cidr = {
    range = "10.10.0.0/20"
  }

  subnet_mask = {
    public  = 4
    private = 4
    publish = 4
  }

  subnet_mask_init = {
    public  = 0
    private = 3
    publish = 6
  }

  create_database_subnet_group = {
    public  = false
    private = true
  }

}
