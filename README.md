## -> SUBNET-PUBLIC-PRIVATE & SUBNET-PUBLIC-PRIVATE-PUBLISH
### InfraCost using 3 AZ's
```
 Name                                                           Monthly Qty  Unit              Monthly Cost 
                                                                                                            
 module.vpc.module.vpc.aws_cloudwatch_log_group.flow_log[0]                                                 
 ├─ Data ingested                                            Monthly cost depends on usage: $0.50 per GB    
 ├─ Archival Storage                                         Monthly cost depends on usage: $0.03 per GB    
 └─ Insights queries data scanned                            Monthly cost depends on usage: $0.005 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[0]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[1]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 module.vpc.module.vpc.aws_nat_gateway.this[2]                                                              
 ├─ NAT gateway                                                         730  hours                   $32.85 
 └─ Data processed                                           Monthly cost depends on usage: $0.045 per GB   
                                                                                                            
 OVERALL TOTAL                                                                                       $98.55 
──────────────────────────────────
50 cloud resources were detected:
∙ 4 were estimated, all of which include usage-based costs, see https://infracost.io/usage-file
∙ 46 were free:
  ∙ 9 x aws_route_table_association
  ∙ 9 x aws_subnet
  ∙ 4 x aws_route
  ∙ 4 x aws_route_table
  ∙ 4 x aws_security_group_rule
  ∙ 3 x aws_eip
  ∙ 3 x aws_security_group
  ∙ 1 x aws_db_subnet_group
  ∙ 1 x aws_default_network_acl
  ∙ 1 x aws_default_route_table
  ∙ 1 x aws_default_security_group
  ∙ 1 x aws_flow_log
  ∙ 1 x aws_iam_policy
  ∙ 1 x aws_iam_role
  ∙ 1 x aws_iam_role_policy_attachment
  ∙ 1 x aws_internet_gateway
  ∙ 1 x aws_vpc

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┓
┃ Project                                                          ┃ Monthly cost ┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━┫
┃ pcnuness/iac/terraform/provider...aws/provider-aws-vpc/terraform ┃ $99          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━┛
```